﻿using RCSwitch;

using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.Networking.Connectivity;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// Il modello di elemento Pagina vuota è documentato all'indirizzo https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x410

namespace Components {
    /// <summary>
    /// Pagina vuota che può essere usata autonomamente oppure per l'esplorazione all'interno di un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage() {
            this.InitializeComponent();

            InitGPIO();

            initRCSwitch();
        }
        


        #region UTILS

        public IPAddress GetIpAddress()
        {
            var hosts = NetworkInformation.GetHostNames();
            foreach (var host in hosts)
            {
                IPAddress addr;
                if (!IPAddress.TryParse(host.DisplayName, out addr)) continue;
                if (addr.AddressFamily != AddressFamily.InterNetwork) continue;
                return addr;
            }
            return null;
        }

        #endregion



        #region GPIO

        Led Led;
        Button Btt;

        private void InitGPIO() {
            Btt = new Button(9);
                Btt.Press += this.Btt_Press;
                Btt.Release += this.Btt_Release;
            Led = new Led(5);

            Unloaded += MainPage_Unloaded;
        }

        private void Btt_Release(object sender, EventArgs e) {
            //Led.On = true;
        }

        private void Btt_Press(object sender, EventArgs e) {
            //Led.On = false;
            //rcRaspberryRemote.SwitchOn("11011", 1);
            rcSwitch.Switch("11011", "10000", true);
            //rcTransmitter.Switch( 'A', 1, true);
            Debug.WriteLine ( "PRESS" );
        }

        private void MainPage_Unloaded(object sender, object args) {
            Led.Dispose();
            Btt.Dispose();
        }

        #endregion
        


        #region RC-SWITCH

        RCSwitchIO rcSwitch;
        RaspberryRemote.RCSwitch rcRaspberryRemote;
        UwpRcTransmitter.RcTransmitter rcTransmitter;


        void initRCSwitch () {

            // TEST RECEIVE
            // https://github.com/michaelosthege/RCSwitch

            // connect sender to GPIO16 and receiver to GPIO27
            rcSwitch = new RCSwitchIO(16, 27);
            //rcSwitch.Protocol = 1;
            //attach the event handler for receiving signals
            rcSwitch.OnSignalReceived += RcSwitch_OnSignalReceived;
            //rcSwitch.RepeatTransmit = 3;
                      

            // TEST TRASMIT
            // https://github.com/Schwenkner/UwpRfTransmitter
            //rcTransmitter = new UwpRcTransmitter.RcTransmitter(17, UwpRcTransmitter.Protocol.REV);



            // TEST TRASMIT
            // https://github.com/Panzenbaby/Raspberry-Remote-for-Windows-10-IoT-Core

            //rcRaspberryRemote = new RaspberryRemote.RCSwitch();
            //rcRaspberryRemote.SetProtocol(1);
            //rcRaspberryRemote.EnableTransmit(17);

        }


        private void RcSwitch_OnSignalReceived(object sender, Signal signal) {
            Debug.WriteLine($"received: {signal.Code} via protocol {signal.Protocol} with bitlength {signal.BitLength}");
        }

        #endregion



        #region INTERFACE

        //private void On_Click(object sender, RoutedEventArgs e)
        //{
        //    //rcTransmitter.Switch('A',1,true);
        //    //rcRaspberryRemote.SwitchOn("11111",1);
        //    //rcSwitch.Switch(codeGroupTB.Text, codeDeviceTB.Text, true);
        //}
        //private void Off_Click(object sender, RoutedEventArgs e)
        //{
        //    rcSwitch.Switch(codeGroupTB.Text, codeDeviceTB.Text, true);
        //}
        //private async void Test_Click(object sender, RoutedEventArgs e)
        //{
        //    System.Diagnostics.Debug.WriteLine("starting test");
        //    for (int i = 0; i < 10; i++) {
        //        System.Diagnostics.Debug.WriteLine(rcSwitch.Switch("11001", "10000", true));
        //        await Task.Delay(1000);
        //        rcSwitch.Switch("11001", "10000", false);
        //        await Task.Delay(1000);
        //    }
        //    System.Diagnostics.Debug.WriteLine("test ended");
        //}

        #endregion

    }

}
