﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.UI.Xaml;

namespace Components {

    public class Led : IDisposable {

        int PinId;
        GpioPin Pin;

        public Led ( int pin ) {
            var gpio = GpioController.GetDefault();
            if (gpio == null){
                Debug.WriteLine ( "There is no GPIO controller on this device." );
                return;
            }
            Pin = gpio.OpenPin(PinId = pin);
            Pin.SetDriveMode(GpioPinDriveMode.Output);
        }

        public bool On {
            get {
                return _On;
            }
            set {
                _On = value;
                Pin.Write ( _On ? GpioPinValue.Low : GpioPinValue.High );
            }
        }
        bool _On = false;


        public void Toggle () {
            On = !On;
        }

        public void Dispose() {
            Pin.Dispose();
        }
    }
}
