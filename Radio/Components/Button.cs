﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.UI.Xaml;

namespace Components {

    public class Button : IDisposable {

        public event EventHandler Press;
        public event EventHandler Release;

        int PinId;
        GpioPin Pin;

        public Button ( int pin ) {
            var gpio = GpioController.GetDefault();
            if (gpio == null){
                Debug.WriteLine ( "There is no GPIO controller on this device." );
                return;
            }
            Pin = gpio.OpenPin(PinId = pin);
            Pin.SetDriveMode(GpioPinDriveMode.Input);
            Pin.ValueChanged += this.Pin_ValueChanged;
        }

        private void Pin_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs args) {
            switch ( args.Edge ) {
                case GpioPinEdge.FallingEdge:
                    Press?.Invoke(this, EventArgs.Empty);
                break;
                case GpioPinEdge.RisingEdge:
                    Release?.Invoke(this, EventArgs.Empty);
                break;
            }
        }

        public void Dispose() {
            Pin.Dispose();
        }
    }
}
